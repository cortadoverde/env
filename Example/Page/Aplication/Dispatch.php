<?php
/**
 * @PSR-0: Aplication\Dispatch
 * ===========================
 *
 * @Filename Dispatch.php
 *
 * @author Pablo Adrian Samudia <p.a.samu@gmail.com>
 */

namespace Env\Aplication;

class Dispatch extends \Env\Object
{
    public static $request;

    public static $response;

    public static function run( \Env\Network\Request $request, \Env\Network\Response $response )
    {
       
        self::$request  = $request;
        self::$response = $response;

        $default = array(
            'namespace' => '\\Env\\Error\\Mising',
            'action'    => 'controller',
            'args'      => array()
        );

        $app = \Env\Route\Route::factory( $request );
        
        if( $app === false ) {
            $app = $default;
        } else {
            if( $app instanceof \Aura\Router\Route ) {
               
                $app = $app->params ;

                if( isset( $app['page'] ) ) {
                    $app['namespace'] .= ucfirst( strtolower( $app['page'] ) );
                }  


            }
            $app = array_merge( $default, $app);
        }

        if( isset( $app['format'] ) ) {
            self::$response->type = trim( $app['format'], '.');
        }

        if( self::call( $app['namespace'], $app['action'], $app['args'] ) === false ) {
            
            trigger_error( $app['namespace'] .' No se encuentra un controlador valido para el manejo de errores, por favor solucionar este problema crando un controlador de Error en la aplicacion', E_USER_ERROR);
        
        }

        $response->render();

    }

    static private function _checkReflection( $namespace )
    {
        try {
            $r = new \ReflectionClass($namespace);
            return $r;
        } catch (\ReflectionException $e) {
            return false;
        }
        return false;
    }

    static public function call( $ns, $action, $args = array() ) 
    {
        if ( ( $r = self::_checkReflection( $ns ) ) !== false ) {

            self::$request->match = array('namespace' => $ns, 'action' => $action );

            if ( $r->isSubclassOf( 'Env\\Controller\\Controller' ) ) {
                $instance = $r->newInstanceArgs( array( self::$request, self::$response ) );
            } else {
                $instance = $r->newInstance();
            }

            // Hook before action
            if( $r->hasMethod('before_action') ){
                $method = $r->getMethod( 'before_action' );
                $method->invoke($instance);
            }

            if( $r->hasMethod($action) ) {
                $method = $r->getMethod( $action );
                if( empty( $args ) ) {
                    return $method->invoke($instance);
                } else {
                    return $method->invokeArgs($instance, $args);
                }
            }

            // Hook before action
            if( $r->hasMethod('after_action') ){
                $method = $r->getMethod( 'after_action' );
                $method->invoke($instance);
            }
        }
        return false;
    }


    public static function shutdown()
    {
        Dispatch::run( \Env\Network\Request::getInstance(), new \Env\Network\Response() );
        //\App\Connection::close();
    }
}

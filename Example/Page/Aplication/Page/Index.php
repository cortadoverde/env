<?php
/**
 * @PSR-0: Env\Aplication\Page\Index
 * =================================
 *
 * @Filename Index.php
 *
 * @author Pablo Adrian Samudia <p.a.samu@gmail.com>
 */

namespace Env\Aplication\Page;

class Index extends \Env\Controller\Controller
{

    public function before_action()
    {

        $this->Page = \Env\View\Page::getInstance();
        $this->Page -> addStylesheet( $this->request->base . '/assets/css/style.css')
                    -> setMetaData( array( 'title' => 'Hola Mundo') );

    }
    
    public function index()
    {
        $this->Page->setMetaData( array( 'title' => 'Titulo de la pagina') );

        // Alias para el template Evn\Aplication\Templates\Layout\Theme.mustache
        // {{> Content }} y cualquier partial puede ser definido gracias al Env\Controller\AliasLoader
        $this->addAlias('Content', 'Page/Index');
        
        $this->data = array(
            array(
                'titulo'    => 'Prueba 0',
                'contenido' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, at, dolor, assumenda itaque quae reprehenderit reiciendis incidunt amet corporis ullam quas tempora iure iste quod commodi laudantium perspiciatis. Provident, molestias.'
            ),
            array(
                'titulo'    => 'Prueba 1',
                'contenido' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, at, dolor, assumenda itaque quae reprehenderit reiciendis incidunt amet corporis ullam quas tempora iure iste quod commodi laudantium perspiciatis. Provident, molestias.'
            ),
            array(
                'titulo'    => 'Prueba 2',
                'contenido' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus, at, dolor, assumenda itaque quae reprehenderit reiciendis incidunt amet corporis ullam quas tempora iure iste quod commodi laudantium perspiciatis. Provident, molestias.'
            )
        );
        
        $this->render();
    }



    

    
}
